% ADF435x driver

# LICENSE #

This ADF435x driver is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This ADF435x driver is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

# DESCRIPTIONS #

ADF435x driver

See Analogdevices webpage.

http://www.analog.com/products/rf-microwave/pll-synth/fractional-n-plls/adf4351.html#product-overview

Datasheet

http://www.analog.com/media/en/technical-documentation/data-sheets/ADF4351.pdf


ADF4350/ADF4351 Evaluation Board Software (Rev. 4.5.0)

http://www.analog.com/media/en/evaluation-boards-kits/evaluation-software/ADF435x_v4_5_0.zip
