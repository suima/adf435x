ECHO     = echo
RM       = rm -f

CC      = gcc
CFLAGS  = -std=gnu11 -W -Wall
LIBS    =
LDFLAGS = -lm

CFLAGS += -O2

.SUFFIXES: .c .o

all: adf435x

.o: %.c
	@$(ECHO) "== compile $@"
	@$(CC) -c -o $(CFLGAS) $<

adf435x.o: adf435x.c adf435x.h
adf435x: adf435x.o
	@$(ECHO) "== link $@"
	@$(CC) -o $@ $(LDFLAGS) $^ $(LIBS)

.PHONY: clean

clean:
	$(RM) *.o
	$(RM) $(ALL)
