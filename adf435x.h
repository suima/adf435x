#ifndef OSC_SET_H_
#define OSC_SET_H_

#include <stdint.h>

/***
 * REG0
 *    [31] Reserved
 * [30:15] 16-bit integer value (INT)
 * [14: 3] 12-bit fractional value (FRAC)
 * [ 2: 0] Control bits
 */
#ifdef __LITTLE_ENDIAN__
typedef struct {
	unsigned reserve    :  1;
	unsigned integer    : 16;
	unsigned fractional : 12;
	unsigned control    :  3;
} bitset0;
#else
typedef struct {
	unsigned control    :  3;
	unsigned fractional : 12;
	unsigned integer    : 16;
	unsigned reserve    :  1;
} bitset0;
#endif

typedef union bitsetConv0 {
	bitset0  bs;
	uint32_t reg;
} bitsetConv0;

/***
 * REG1
 * [31:29] Reserved
 *    [28] Phase adjust
 *    [27] Prescaler
 * [26:15] 12-bit phase value (PHASE)
 * [14: 3] 12-bit Modulus value (MOD)
 * [ 2: 0] Control bits
 */
#ifdef __LITTLE_ENDIAN__
typedef struct {
	unsigned reserve   :  3;
	unsigned phase     :  1;
	unsigned prescaler :  1;
	unsigned phase_val : 12;
	unsigned mod_val   : 12;
	unsigned control   :  3;
} bitset1;
#else
typedef struct {
	unsigned control   :  3;
	unsigned mod_val   : 12;
	unsigned phase_val : 12;
	unsigned prescaler :  1;
	unsigned phase     :  1;
	unsigned reserve   :  3;
} bitset1;
#endif

typedef union {
	bitset1  bs;
	uint32_t reg;
} bitsetConv1;

/***
 * REG2
 *    [31] Reserved
 * [30:29] Low noise and low spur mode
 * [28:26] Mixout
 *    [25] Reference doubler DBR
 *    [24] RDIV2 DBR
 * [23:14] 10-bit Cuonter DBR
 *    [13] Double buffer
 * [12: 9] Charge pump current setting
 *     [8] LDF
 *     [7] LDP
 *     [6] PD Polarity
 *     [5] Power Down
 *     [4] CP Three State
 *     [3] Counter Reset
 * [ 2: 0] Control bits
 */
#ifdef __LITTLE_ENDIAN__
typedef struct {
	unsigned reserve   : 1;
	unsigned low_noise : 2;
	unsigned muxout    : 3;
	unsigned reference : 1;
	unsigned rdiv2     : 1;
	unsigned counter   : 10;
	unsigned buffer    : 1;
	unsigned charge    : 4;
	unsigned ldf       : 1;
	unsigned ldp       : 1;
	unsigned polarity  : 1;
	unsigned pwr_down  : 1;
	unsigned cp3state  : 1;
	unsigned counet_rst: 1;
	unsigned control   : 3;
} bitset2;
#else
typedef struct {
	unsigned control   : 3;
	unsigned counet_rst: 1;
	unsigned cp3state  : 1;
	unsigned pwr_down  : 1;
	unsigned polarity  : 1;
	unsigned ldp       : 1;
	unsigned ldf       : 1;
	unsigned charge    : 4;
	unsigned buffer    : 1;
	unsigned counter   : 10;
	unsigned rdiv2     : 1;
	unsigned reference : 1;
	unsigned muxout    : 3;
	unsigned low_noise : 2;
	unsigned reserve   : 1;
} bitset2;
#endif

typedef union {
	bitset2  bs;
	uint32_t reg;
} bitsetConv2;

/* REG3
 * [31:24] Reserved
 *    [23] Band select clock mode
 *    [22] ABP
 *    [21] Charge cancel
 * [20:19] Reserved
 *    [18] CSR
 *    [17] Reserved
 * [16:15] Clock div mode
 * [14: 3] 12-bit Clock divider value
 * [ 2: 0] Control bits
 */

/* REG4
 * [31:24] Reserved
 *    [23] Feedback select
 * [22:20] RF divider select
 * [19:12] 8-bit band select clock divider value
 *    [11] VCD Power down
 *    [10] Mute Till Lock Detect
 *    [ 9] AUX output select
 *    [ 8] AUX output enable
 * [ 7: 6] AUX output power
 *    [ 5] RF output enable
 * [ 4: 3] Output power
 * [ 2: 0] Control bits
 */
#ifdef __LITTLE_ENDIAN__
typedef struct {
	unsigned reserve   : 8;
	unsigned feedback  : 1;
	unsigned rf_divider: 3;
	unsigned band      : 8;
	unsigned vcc_power : 1;
	unsigned mtld      : 1;
	unsigned aux_sel   : 1;
	unsigned aux_en    : 1;
	unsigned aux_power : 2;
	unsigned rf_en     : 1;
	unsigned out_power : 2;
	unsigned control   : 3;
} bitset4;
#else
typedef struct {
	unsigned control   : 3;
	unsigned out_power : 2;
	unsigned rf_en     : 1;
	unsigned aux_power : 2;
	unsigned aux_en    : 1;
	unsigned aux_sel   : 1;
	unsigned mtld      : 1;
	unsigned vcc_power : 1;
	unsigned band      : 8;
	unsigned rf_divider: 3;
	unsigned feedback  : 1;
	unsigned reserve   : 8;
} bitset4;
#endif

typedef union bitsetConv4 {
	bitset4  bs;
	uint32_t reg;
} bitsetConv4;

/* REG5
 * [31:24] Reserved
 * [23:22] LD pin mode
 *    [21] Reserved "0"
 * [20:19] Reserved , ALL "1"
 * [18: 3] Reserved , All "0"
 * [ 2: 0] Control bits
 */

typedef enum {
	ADF4351_RFDIV_1,
	ADF4351_RFDIV_2,
	ADF4351_RFDIV_4,
	ADF4351_RFDIV_8,
	ADF4351_RFDIV_16,
	ADF4351_RFDIV_32,
	ADF4351_RFDIV_64,
} ADF4351_RFDIV_t;

#endif
