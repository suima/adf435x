#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "adf435x.h"

#define  OSC_RF_REF 10.00  // 10MHz
#define  OSC_RF_RES 0.0250 // 25kHz

#define  OSC_REG2 (0x00004E42)
#define  OSC_REG3 (0x008004B3)
#define  OSC_REG5 (0x00580005)

uint32_t gcd_iter(uint32_t u, uint32_t v) {
	uint32_t t;
	while (v) {
		t = u;
		u = v;
		v = t % v;
	}
	return u;
}

ADF4351_RFDIV_t ADF4351_Select_Output_Divider(double RFoutFrequency) {
	// NOTE: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/drivers/iio/frequency/adf4350.c?id=HEAD
	// Select divider
	if (RFoutFrequency >= 2200.0) return ADF4351_RFDIV_1;
	if (RFoutFrequency >= 1100.0) return ADF4351_RFDIV_2;
	if (RFoutFrequency >=  550.0) return ADF4351_RFDIV_4;
	if (RFoutFrequency >=  275.0) return ADF4351_RFDIV_8;
	if (RFoutFrequency >=  137.5) return ADF4351_RFDIV_16;
	                              return ADF4351_RFDIV_32;
}

/**
 *  See ADF4351 Datasheet P14 - P17
 */
int main (int argc, char *argv[]) {
	if (argc != 3) {
		printf("\n"
		       "Usage  : %s <OSC Number> <freq [kHz]> \n", argv[0]);
		printf("Example: Set OSC 0 , 321[MHz] \n");
		printf("         %s 0 321000 \n", argv[0]);
		printf("Example: Set OSC 1 , 456.7[MHz] \n");
		printf("         %s 1 456.7 \n", argv[0]);
		return -1;
	}

	int8_t sel_osc;
	sel_osc = atoi(argv[1]);
	if((sel_osc == 0) || (sel_osc == 1) || (sel_osc == 2)) {
		printf("RF Refarence  : %7.3f [MHz]\n",OSC_RF_REF);
		printf("RF Resolution : %7.3f [MHz]\n",OSC_RF_RES);
		printf("Set OSC_%d     : ",sel_osc);
	} else {
		fprintf(stderr,"Error: unexpected OSC number\n");
		fprintf(stderr,"You need select OSC 0 / 1 / 2\n");
		return -1;
	}
	double RFout = atof(argv[2]);
	printf("%f[MHz]\n",RFout);

	// NOTE: Reg4.Feedback == 1
	uint16_t DIV = ADF4351_Select_Output_Divider(RFout);
	uint16_t OSC_DIV = (1U<<DIV);
	double N = ((RFout * OSC_DIV) / OSC_RF_REF);
	uint16_t Integer = (uint16_t)round(N);
	uint16_t Mod  = (uint16_t)(round(OSC_RF_REF / OSC_RF_RES));
	uint16_t Frac = (uint16_t)(round(((double)N - Integer) * Mod));
	uint32_t D    = gcd_iter((uint32_t)Mod, (uint32_t)Frac);
	Mod  = Mod / D;
	Frac = Frac / D;

	if(Mod == 1)
		Mod = 2;

	if(Integer < 22) {
		fprintf(stderr,
			"Error: unexpected OSC frequency(integer value)\n"
			"NOT ALLOWED integer value range 0 ~ 22.\n"
			"Integer %d\n",Integer);
		return -1;
	}

	if(Frac > 4095) {
		fprintf(stderr,
			"Error: unexpected OSC frequency(frac value)\n"
			"NOT ALLOWED integer value over 4095.\n"
			"Frac %d\n",Frac);
		return -1;
	}

	const uint32_t reg2 = OSC_REG2; // FIX
	const uint32_t reg3 = OSC_REG3; // FIX
	const uint32_t reg5 = OSC_REG5; // FIX

	// NOTE: bitsetConv0 conv0;
	uint32_t reg0;
	bitsetConv0 conv0;
	conv0.bs.reserve    = 0;
	conv0.bs.integer    = Integer;
	conv0.bs.fractional = Frac;
	conv0.bs.control    = 0b000;
	reg0 = conv0.reg;

	// NOTE: bitsetConv1 conv1;
	uint32_t reg1;
	bitsetConv1 conv1;
	conv1.bs.reserve   = 0b000;
	conv1.bs.phase     = 0b0;
	conv1.bs.prescaler = 0b1;
	conv1.bs.phase_val = 0x001; // Recommended
	conv1.bs.mod_val   = Mod;
	conv1.bs.control   = 0b001;
	reg1 = conv1.reg;

	// NOTE: bitsetConv4 conv4;
	uint32_t reg4;
	bitsetConv4 conv4;
	conv4.bs.reserve   = 0x00;
	conv4.bs.feedback  = 0b1;
	conv4.bs.rf_divider= DIV;
	conv4.bs.band      = 0x14;
	conv4.bs.vcc_power = 0b0;
	conv4.bs.mtld      = 0b0;
	conv4.bs.aux_sel   = 0;
	conv4.bs.aux_power = 0b00;
	conv4.bs.rf_en     = 0b1;
	conv4.bs.out_power = 0b11;
	conv4.bs.control   = 0b100;
	reg4 = conv4.reg;

	printf("MOD           : %3d [MHz]\n",Mod);
	printf("Integer       : %3d\n",Integer);
	printf("Frac          : %3d\n",Frac);
	printf("---\n");
	printf("REG2          : 0x%08X\n",reg2);
	printf("REG3          : 0x%08X\n",reg3);
	printf("REG5          : 0x%08X\n",reg5);
	printf("---\n");
	printf("REG0          : 0x%08X\n",reg0);
	printf("REG1          : 0x%08X\n",reg1);
	printf("REG4          : 0x%08X\n",reg4);

	return 0;
}
